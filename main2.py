countries = ['Ukraine', 'Spain', 'Italy']

capitals = {
    'Ukraine': 'Kyiv',
    'Spain': 'Madrid',
    'Italy': 'Rome'
}

for country, capital in capitals.items():
    print("{0}: {1}".format(country, capital))
