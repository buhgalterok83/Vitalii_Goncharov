num1 = int(input("Enter first number: "))
num2 = int(input("Enter second number: "))

sum = num1 + num2
prod = num1 * num2

print("{0} + {1} = {2}".format(num1, num2, sum))
print("{0} * {1} = {2}".format(num1, num2, prod))

